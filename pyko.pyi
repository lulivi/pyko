import os
from typing import Optional

class KanOrgError(Exception):
    """Base Kanban Organizer error."""

class KanOrgBoard:
    """KanOrg board representation.

    :param target_dir: base directory in where the configuration will be stored. If not provided
        it will use the current working directory.
    """
    def __init__(self, target_dir: str = os.getcwd()) -> None: ...

    @staticmethod
    def create(target_path: str) -> None:
        """Creates a new configuration in the target path.

        :param target_path: the path in where the configuration will be created.
        """

    def show(self, workflow_name: Optional[str]) -> str:
        """Shows the current state of the board.

        :param workflow_name: Name of the desired workflow to print. If it is empty, the full board
            will be printed instead.
        :returns: The workflow or the full board string representation.
        """

    def add(self, title: str, workflow_name: Optional[str]) -> None:
        """Adds a new task to a workflow if specified.

        :param title: New task title.
        :param workflow_name: The name of the workflow in where place the new task.
        """

    def relocate(self, task: str, workflow_name: str = "backlog") -> None:
        """Moves a task to other workflow.

        :param task: Task ID to move.
        :param workflow_name: Workflow where the task will be moved. If no worklow is provided, the
            task will be moved to the "backlog" workflow.
        """

    def delete(self, task: str) -> None:
        """Deletes a task.

        :param task: Task ID to delete.
        """
