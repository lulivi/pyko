extern crate kanorg;
extern crate pyo3;

use pyo3::{create_exception, exceptions, prelude::*};
use std::path::PathBuf;

create_exception!(pyko, KanOrgError, exceptions::PyException);

/// Creates a new object to be used in each KanOrgBoard instance method.
///
/// # Arguments:
///
/// * `target_dir` - base directory where the [`KanOrgBoard`] configuration is located.
///
/// [`KanOrgBoard`]: kanorg::board::KanOrgBoard
fn ko_new(target_dir: &String) -> PyResult<kanorg::board::KanOrgBoard> {
    match kanorg::board::KanOrgBoard::new(&PathBuf::from(target_dir)) {
        Ok(new_obj) => Ok(new_obj),
        Err(err) => Err(KanOrgError::new_err(err.to_string())),
    }
}

#[pyclass]
struct KanOrgBoard {
    target_dir: String,
}

#[pymethods]
impl KanOrgBoard {
    #[new]
    fn new(target_dir: Option<String>) -> Self {
        Self {
            target_dir: target_dir.unwrap_or(
                std::env::current_dir()
                    .expect("Could not get the current directory")
                    .to_str()
                    .expect("Could not convert the current directory to String")
                    .to_owned(),
            ),
        }
    }

    /// Create a new configuration in the current directory.
    #[staticmethod]
    fn create(target_path: &str) -> PyResult<()> {
        if let Err(err) = kanorg::board::KanOrgBoard::create(target_path) {
            return Err(KanOrgError::new_err(err.to_string()));
        }

        Ok(())
    }

    fn show(&self, workflow_name: Option<&str>) -> PyResult<String> {
        let mut stdout_capture = std::io::Cursor::new(Vec::new());

        if let Err(err) = ko_new(&self.target_dir)?.show(workflow_name, &mut stdout_capture) {
            return Err(KanOrgError::new_err(err.to_string()));
        }

        Ok(std::str::from_utf8(&stdout_capture.into_inner())?.to_owned())
    }

    fn add(&self, title: &str, workflow_name: Option<&str>) -> PyResult<()> {
        if let Err(err) = ko_new(&self.target_dir)?.add(title, workflow_name, false) {
            return Err(KanOrgError::new_err(err.to_string()));
        }

        Ok(())
    }

    fn relocate(&self, task: &str, workflow_name: Option<&str>) -> PyResult<()> {
        if let Err(err) = ko_new(&self.target_dir)?.relocate(task, workflow_name) {
            return Err(KanOrgError::new_err(err.to_string()));
        }

        Ok(())
    }

    fn delete(&self, task: &str) -> PyResult<()> {
        if let Err(err) = ko_new(&self.target_dir)?.delete(task) {
            return Err(KanOrgError::new_err(err.to_string()));
        }

        Ok(())
    }
}

/// Python bindings for KanOrg commandline utility.
#[pymodule]
fn pyko(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add("KanOrgError", _py.get_type::<KanOrgError>())?;
    m.add_class::<KanOrgBoard>()?;

    Ok(())
}
