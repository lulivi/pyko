from pyko import KanOrgBoard, KanOrgError
import subprocess
from pathlib import Path

CURRENT_DIR: Path = Path(__file__).resolve().parent
KANORG_BASE_DIR: Path = CURRENT_DIR.joinpath("juanito")

def main():
    subprocess.run(["mkdir", str(KANORG_BASE_DIR)])
    a: KanOrgBoard = KanOrgBoard(str(KANORG_BASE_DIR))

    a.create(str(KANORG_BASE_DIR))
    print(a.show())
    a.add("This is a new task", "doing")
    print(a.show("doing"))
    a.relocate("1", "todo")
    print(a.show())

    input("Press enter to finish")
    subprocess.run(["rm", "-r", str(KANORG_BASE_DIR)])

if __name__ == "__main__":
    main()
