FROM python:3.9

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Europe/Paris
ARG user

RUN apt-get update && \
    apt-get install -y cargo rustc && \
    rm -rf /var/lib/apt/lists/* && \
    update-ca-certificates && \
    pip3 install maturin

RUN mkdir -p /code && chown $user /code
WORKDIR /code
USER $user
COPY --chown=$user . .
RUN mkdir -p /code/target/wheels/
VOLUME /code/target/wheels/

CMD ["maturin", "build", "--release"]
