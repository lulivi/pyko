default: build

IMAGE_TAG := pyko-builder
IMAGE_USER := $(shell id -u)

x64:
	.venv/bin/maturin build --release
	cp target/wheels/* wheels/

arm:
	mkdir -p .cargo && cargo vendor > .cargo/config 2>/dev/null
	docker build --build-arg user="$(IMAGE_USER)" --platform=linux/arm/v7 -t "$(IMAGE_TAG)" .
	docker run --rm -v "$(PWD)/wheels/:/code/target/wheels/" -u "$(IMAGE_USER)" "$(IMAGE_TAG)"